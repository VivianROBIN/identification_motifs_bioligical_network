import networkx as nx
import numpy as np
import time
import pandas as pd
from load_graph_motifs import load_motif_list
import glob
import os
import re
import threading
import multiprocessing

class MotifAnalyzer:
    def __init__(self, graph_motif_list):
        self.graph_motif_list = graph_motif_list
        self.G = None  # Store the input graph

    def load_graph_from_csv(self, csv_file, source_column, target_column):
        '''
        Load the input graph from a CSV file.

        :param csv_file: str, path to the CSV file.
        :param source_column: str, column name for the source nodes.
        :param target_column: str, column name for the target nodes.
        '''
        self.G = None
        self.G = nx.from_pandas_edgelist(pd.read_csv(csv_file, sep="\t", header=None), source_column, target_column)  # Use source_column and target_column as node identifiers.

    def calc_motif_distribution_with_node_indices(self):
        '''
        Calculate the motif distribution of the stored graph and return node indices for each motif.

        :return: hist: np.array(int) represents the occurrence of graph motifs in param graph_motif_list.
        :return: node_indices: List[List[int]] represents the node indices for each motif.
        '''
        if self.G is None:
            raise ValueError("Graph not loaded. Please call load_graph_from_csv first.")

        hist = np.zeros(len(self.graph_motif_list), dtype=int)
        node_indices = []  # Create a list to store node indices for each motif

        for index, motif in enumerate(self.graph_motif_list):
            if motif.number_of_nodes() == 1:
                hist[index] = self.G.number_of_nodes()
            elif motif.number_of_nodes() == 2:
                hist[index] = self.G.number_of_edges()
            else:
                GM = nx.algorithms.isomorphism.GraphMatcher(self.G, motif)
                indices_for_motif = []  # Store node indices for the current motif
                for subgraph in GM.subgraph_isomorphisms_iter():
                    hist[index] += 1
                    indices_for_motif.extend(subgraph.keys())  # Collect node indices
                node_indices.append(indices_for_motif)  # Add node indices for the current motif to the list

        print("Node Indices for Each Motif:")
        for idx, indices in enumerate(node_indices):
            print(f"Motif {idx}: {indices}")

        return hist, node_indices



def extract_network_name(file_name):
    # Utilisez une expression régulière pour extraire le nom de réseau souhaité
    # HT et NHT
    match = re.search(r'Subnetwork_(\w+)\.txt', file_name)
    # random
    
    #match = re.search(r'Subnetwork_(\w+_\d+)\.csv', file_name)
    #match = re.search(r'(.+)\.csv', file_name)
    if match:
        return match.group(1)
    else:
        return "Unknown"

def process_file(f, counter, lock, output_file):
    file_size = os.path.getsize(f)
    if file_size <= 500000:
        motif_analyzer.load_graph_from_csv(f, source_column=0, target_column=1)
        
        num_nodes = motif_analyzer.G.number_of_nodes()
        num_edges = motif_analyzer.G.number_of_edges()
        
        motifs_all_medicament = motif_analyzer.calc_motif_distribution_with_node_indices()
        
        network_name = np.array([extract_network_name(os.path.basename(f))], dtype='<U64')
        
        result = np.concatenate((network_name, np.array([num_nodes, num_edges]), motifs_all_medicament))
        
        with lock:
            counter.value += 1
            print(f"Processed {counter.value}/{len(files)} files")

        with lock:
            # Écrivez le résultat ligne par ligne dans le fichier
            output_file.write("\t".join(result) + "\n")
            output_file.flush()

if __name__ == "__main__":
    motif_list = load_motif_list([3, 4])
    path = "/mnt/projects_tn01/AD_Reseau_Proteome/Commun_arrangement_PPI/Results/Local_Pairwise_alignement/Sub_network_sign_Hepa_none_Hep/"
    files = glob.glob(path + "/*.txt")
    files = sorted(files)
    output_directory = "/mnt/projects_tn01/AD_Reseau_Proteome/Commun_arrangement_PPI/Results/Motifs_Network/Protein_motifs/"
    output_file_path = "/mnt/projects_tn01/AD_Reseau_Proteome/Commun_arrangement_PPI/Results/Motifs_Network/motifs_Drug_Global_size_3_4_bis.csv"

    for f in files:
        motif_analyzer = MotifAnalyzer(motif_list)
        motif_analyzer.load_graph_from_csv(f, source_column=0, target_column=1)
        network_name = extract_network_name(f)

        for index, motif in enumerate(motif_list):
            motif_analyzer.graph_motif_list = [motif]
            motif_distribution, node_indices = motif_analyzer.calc_motif_distribution_with_node_indices()

            motif_proteins = set()  # Utilisez un ensemble pour éviter les doublons
            for indices in node_indices:
                motif_proteins.update(map(str, indices))

            motif_file_path = os.path.join(output_directory, f"{network_name}_Motif_{index + 1}.txt")
            with open(motif_file_path, 'w') as motif_file:
                motif_file.write("\n".join(motif_proteins) + "\n")
            print(f"Processed {network_name} Motif {index + 1}")

    print(f"{len(files)} files have been processed, and the results have been saved to {output_directory}.")
