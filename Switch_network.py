import pandas as pd
import numpy as np
import networkx as nx
import random
import os
import re
import glob
from multiprocessing import Pool

def extract_network_name(file_name):
    # Utilize a regular expression to extract the desired network name
    match = re.search(r'Subnetwork_(\w+)\.txt', file_name)
    if match:
        return match.group(1)
    else:
        return "Unknown"

def process_file(file_data):
    graph_motif_list, num_networks, output_dir, density_tolerance, idx = file_data
    G = nx.read_edgelist(graph_motif_list, delimiter="\t")

    num_nodes = len(G.nodes())
    num_edges = len(G.edges())
    target_density = num_edges / ((num_nodes * (num_nodes - 1)) / 2)

    random_network_count = 0  # Count of random networks generated

    print(f"Chargement du fichier {idx + 1}/{len(files)} : {graph_motif_list}")

    while random_network_count < num_networks:
        # Create a random network with the same number of nodes as the original network
        random_network = nx.Graph()
        random_network.add_nodes_from(G.nodes())

        # Calculate the number of edges needed to reach the same number of interactions as the original network
        num_edges_needed = num_edges

        # Add random edges to reach the target density
        nodes_list = list(random_network.nodes())
        while len(random_network.edges()) < num_edges_needed:
            node1, node2 = random.sample(nodes_list, 2)
            if not random_network.has_edge(node1, node2):
                random_network.add_edge(node1, node2)

        # Save the random network
        network_name = extract_network_name(os.path.basename(graph_motif_list))
        random_network_file = f"{output_dir}/{network_name}_random_{random_network_count}.csv"
        nx.write_edgelist(random_network, random_network_file, delimiter="\t", data=False)
        random_network_count += 1

    print(f"Ensemble de réseaux aléatoires générés pour le fichier {idx + 1}/{len(files)} : {graph_motif_list}")

if __name__ == "__main__":
    path = "/mnt/projects_tn01/AD_Reseau_Proteome/Commun_arrangement_PPI/Results/Local_Pairwise_alignement/Sub_network_sign_Hepa_none_Hep/"
    files = glob.glob(path + "/*.txt")

    # Sort the list of files alphabetically
    files = sorted(files)

    output_directory = "/mnt/projects_tn01/AD_Reseau_Proteome/Commun_arrangement_PPI/Results/Local_Pairwise_alignement/Sub_network_sign_Hepa_none_Hep/Drug_random_network/"

    num_networks = 1000  # Générer un maximum de 10 réseaux aléatoires
    density_tolerance = 0.1  # Tolerance de densité (ajustez selon vos besoins)

    # Create a list of data for multiprocessing
    file_data_list = [(file, num_networks, output_directory, density_tolerance, idx) for idx, file in enumerate(files)]

    # Create a pool of worker processes and distribute the work
    with Pool(processes=25) as pool:  # Adjust the number of processes as needed
        pool.map(process_file, file_data_list)
